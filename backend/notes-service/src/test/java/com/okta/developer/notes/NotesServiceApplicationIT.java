package com.okta.developer.notes;

import com.okta.developer.notes.service.NotesService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

public class NotesServiceApplicationIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NotesService notesService;

    @Test
    public void contextLoads() {}
}
