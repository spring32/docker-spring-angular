package com.okta.developer.notes.service.impl;

import com.okta.developer.notes.repository.NotesRepository;
import com.okta.developer.notes.service.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotesServiceImpl implements NotesService {

    private NotesRepository notesRepository;

    @Autowired

    public NotesServiceImpl(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }
}
